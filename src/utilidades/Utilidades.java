/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidades;

import java.util.Scanner;

/**
 *
 * @author nacho
 */
public class Utilidades {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
    public static int validarEntero(String mensaje) {
        boolean salir=false;
        int enteroValidado=0;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(mensaje);
                enteroValidado=sc.nextInt();
                salir=true;
            } catch (Exception e) {
                System.out.println("Opción no válida");
            } finally {
                sc.nextLine(); // Vacío buffer scanner
            }
        } while (!salir);
        return enteroValidado;
    }
    
    public static int validarEntero(String mensaje, int min, int max) {
        boolean salir=false;
        int enteroValidado=0;
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(mensaje);
                enteroValidado=sc.nextInt();
                if (enteroValidado>=min && enteroValidado<=max) {
                    salir=true;
                } else {
                    System.out.println("El número ha de estar comprendido entre " + min + " y " + max);
                }
            } catch (Exception e) {
                System.out.println("Opción no válida");
            } finally {
                sc.nextLine(); // Vacío buffer scanner
            }
        } while (!salir);
        return enteroValidado;
    }
        
    public static String validarString(String mensaje, int numCaracteresMax) {
        boolean salir=false;
        String cadenaValidada="";
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print(mensaje);
                cadenaValidada=sc.next();
                if (cadenaValidada.length()<=numCaracteresMax) {
                    salir=true;
                } else {
                    System.out.println("El número de caracteres máximo es " + numCaracteresMax);
                }
            } catch (Exception e) {
                System.out.println("Opción no válida");
            }
        } while (!salir);
        return cadenaValidada;
    }        
    
    public static void pausar() {
        Scanner sc = new Scanner(System.in);
        System.out.print("<Pulse intro para continuar> ");
        sc.nextLine();
    }
    
    static public double redondear(double num, int decimales) {
        double numDividir=Math.pow(10, decimales);
        double n=num*numDividir;
        n=Math.round(n);
        return n/numDividir;
    }
}
